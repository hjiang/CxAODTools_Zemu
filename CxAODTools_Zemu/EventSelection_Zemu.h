// Dear emacs, this is -*-c++-*-
#ifndef CxAODTools__EventSelection_Zemu_H
#define CxAODTools__EventSelection_Zemu_H

#include "CxAODTools/EventSelection.h"
#ifndef __MAKECINT__
#include "CxAODTools/CommonProperties.h"
#include "CxAODTools/CutFlowCounter.h"
#endif // not __MAKECINT__

#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
#include "EventLoop/StatusCode.h"

#include <TString.h>

// structure holding kinematics of the result
struct ZemuResult
{
  bool pass;
  std::vector<const xAOD::Electron*> el;
  std::vector<const xAOD::Muon*> mu;

  std::vector<const xAOD::Jet*> jets;
  std::vector<const xAOD::TauJet*> taus;

  const xAOD::MissingET* met;

  ///TruthInformation
  std::vector<const xAOD::TruthParticle*> truthParticles;

};


class EventSelection_Zemu : public EventSelection
{

public:
    ZemuResult m_result;

    EventSelection_Zemu(const TString cutFlowName = "PreselectionCutFlow_Zemu");
    ~EventSelection_Zemu() = default;

    virtual bool passPreSelection(SelectionContainers & containers, bool isKinVar) override;
    virtual bool passSelection(SelectionContainers & containers, bool isKinVar) override;

protected:
    virtual void clearResult(); //Resets the variables in m_result


    int doLeptonPreSelection(const xAOD::ElectronContainer* electrons,
                           const xAOD::MuonContainer* muons,
                           std::vector<const xAOD::Electron*>& el,
                           std::vector<const xAOD::Muon*>& mu);

    int doJetPreSelection(const xAOD::JetContainer* jets);
    int doTauPreSelection(const xAOD::TauJetContainer* taus);
    //int getTruthInformation(const xAOD::TruthParticleContainer* truthParticles);

};

#endif






