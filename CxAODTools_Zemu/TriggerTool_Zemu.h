#ifndef CxAODTools_VHbb_TriggerTool_Zemu_H
#define CxAODTools_VHbb_TriggerTool_Zemu_H

#include "CxAODTools/TriggerTool.h"

class TriggerTool_Zemu : public TriggerTool {

public:

  TriggerTool_Zemu(ConfigStore& config);
  virtual ~TriggerTool_Zemu() = default;

  //It is necessary to override all of the following functions
  virtual EL::StatusCode initTools() override;
  virtual EL::StatusCode initProperties() override;
  virtual EL::StatusCode initTriggers() override;

  void addLowestUnprescaledElectron() override;

};

#endif // ifndef CxAODTools_VHbb_TriggerTool_Zemu_H
