#include "CxAODTools_Zemu/TriggerTool_Zemu.h"

#include "CxAODTools/CommonProperties.h"

//#include "xAODMissingET/MissingETContainer.h"
//#include "xAODMissingET/MissingETAuxContainer.h"

TriggerTool_Zemu::TriggerTool_Zemu(ConfigStore& config) :
  TriggerTool(config)
{
}//end constructor

EL::StatusCode TriggerTool_Zemu::initTools() {
  EL_CHECK("TriggerTool_Zemu::initTools()", initMuonSFTool("2015", "Medium"));
  EL_CHECK("TriggerTool_Zemu::initTools()", initMuonSFTool("2016", "Medium"));
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TriggerTool_Zemu::initProperties() {
  m_electronEffProp = &Props::trigEFFmediumLHIsoGradient;
  m_electronSFProp  = &Props::trigSFmediumLHIsoGradient;
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode TriggerTool_Zemu::initTriggers() {

    addLowestUnprescaledElectron();
    addLowestUnprescaledMuon();
    return EL::StatusCode::SUCCESS;
}

void TriggerTool_Zemu::addLowestUnprescaledElectron() {
  ADD_TRIG_MATCH(HLT_e24_lhmedium_L1EM20VH, any,  data15, data15);
  ADD_TRIG_MATCH(HLT_e60_lhmedium,          any,  data15, data15);

  //ADD_TRIG_MATCH(HLT_e26_lhtight_nod0_ivarloose, any,  data16A, data16F2L11);
  //ADD_TRIG_MATCH(HLT_e60_lhmedium_nod0,          any,  data16A, data16F2L11);
  //ADD_TRIG_MATCH(HLT_e60_medium,                 any,  data16A, data16F2L11);
}//end addLowestUnprescaledElectron




