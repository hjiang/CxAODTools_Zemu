#include "CxAODTools_Zemu/EventSelection_Zemu.h"

EventSelection_Zemu::EventSelection_Zemu(const TString cutFlowName ) :
    EventSelection::EventSelection(cutFlowName){
}

// Attention!
// the CxAODMaker AnalysisBase.cxx only do the passPreSelection(SelectionContainers & containers, bool isKinVar) defined here 
// see the calling details in EventSelector_Zemu.cxx
bool EventSelection_Zemu::passPreSelection(SelectionContainers & containers, bool isKinVar){
    if(!isKinVar) m_cutFlow.count("All Events");
    const xAOD::EventInfo* evtinfo = containers.evtinfo;
    clearResult();

    //printf("\n \n Here, is my own EventSelection_Zemu::passPreSelection(SelectionContainers & containers, bool isKinVar) \n \n \n");
	//isKinVar = false;

    bool pass = true;
    int isMC = Props::isMC.get(evtinfo);

    ///Require that the event has a primary vertex.
	// both the data and mc has the hasPV property
    pass &= Props::hasPV.get(evtinfo);
    if(!pass) return false; //Don't bother reading this event if there is no PV
    if(!isKinVar) m_cutFlow.count("Primary Vertex");
    //printf("\n in EventSelection_Zemu::passPreSelection, after the Props::hasPV cut \n");

	//std::cout<< " pass after PV: " << pass << std::endl;

    ///Require that the data event pass the GRL and have no
    /// known detector errors.
    if (!isMC) {
        pass &= Props::passGRL.get(evtinfo);
        pass &= Props::isCleanEvent.get(evtinfo); //This seems okay - it just checks that none of the detector systems were reading errors.
        //std::cout << "Checking GRL" << std::endl;
    }//end if : check the Good runs list
    //std::cout<< " pass after GRL and CleanEvent: " << pass << std::endl;
    if (!pass) return false;
    if(!isKinVar) m_cutFlow.count("Event Cleaning");
	//printf("\n in EventSelection_Zemu::passPreSelection, after the Props::passGRL and Props::isCleanEvent cut \n");


	// here put the selected(passed doLeptonPreSelection) electrons and muons into "electrons" and "muons"
    std::vector<const xAOD::Electron*> electrons;
    std::vector<const xAOD::Muon*> muons;
    int nLeptons = doLeptonPreSelection(containers.electrons, containers.muons, electrons, muons);

    // Require that the event have at least 1 lepton
    // which passes the loose selection criteria.
    // if( nLeptons < 1 ) return false;
    if(!isKinVar) m_cutFlow.count("Loose Lepton");
	//printf("\n in EventSelection_Zemu::passPreSelection, after the doLeptonPreSelection() cut \n \n");

    // fill lepton result
    for(const xAOD::Electron* thisEl : electrons){
        m_result.el.push_back(thisEl);
    }//end for : add all electrons to the event.
    for (const xAOD::Muon* thisMu : muons){
        m_result.mu.push_back(thisMu);
    }//end for : add all muons to the event.

    //Do not use any MET selection on this event.
    //std::cout << "Putting MET CONTAINERS into ZemuResult. " << std::endl;
    m_result.met = containers.met;
    if(!containers.met) std::cout << "ERROR : MET Container not initialized." << std::endl;
    if(!pass) return false;
    if(!isKinVar) m_cutFlow.count("MET");

    // jet cleaning
    //Do not do ANY jet cleaning
    //pass &= doJetCleaning(containers.jets);
    if(!containers.jets) std::cout << "ERROR : jets Container not initialized." << std::endl;
    if (!pass) return false;
    if(!isKinVar) m_cutFlow.count("jet cleaning");

    // jets selection + sorting.  Don't do any cutting based on jets
    int nJets = doJetPreSelection(containers.jets); // the first jet[0] is always the leading jet
    if (!pass) return false;
    if(!isKinVar) m_cutFlow.count("jets");

	// retrieve truth particles
   	//if(!containers.truthParticles) std::cout << "ERROR : truthParticles Container not initialized." << std::endl;
    //m_result.taus = containers.taus;
	//int nTaus = doTauPreSelection(containers.taus);
    //if (!pass) return false;
    //if(!isKinVar) m_cutFlow.count("retrieve taus");

    m_result.pass = true;
    return true;
}//end passPreSelection function


// Attention!
// both passPreSelection() and passSelection() functions are called in the default AnalysisReader.cxx, the CxAODMaker only do the passPreSelection()
bool EventSelection_Zemu::passSelection(SelectionContainers & containers, bool isKinVar){
    // do the pre-selection
    //bool pass = passPreSelection(containers, isKinVar);
    //std::cout << "pass? : " << pass << std::endl;
    //m_result.pass = false;

	// only for check the Props::isLooseLH of electrons
	/*
    for (unsigned int iElec = 0; iElec < m_result.el.size(); ++iElec) {
        const xAOD::Electron* elec = m_result.el[iElec];
		printf( "Here is a check inside EventSelection_Zemu::passSelection, Props::isLooseLH:  %i \n", Props::isLooseLH.get(elec));
        //nelecs++;
        //el.push_back(elec);
    }
	*/

	/*
    // get containers
    const xAOD::EventInfo* evtinfo = containers.evtinfo;
    bool isMC = Props::isMC.get(evtinfo);

    bool passTriggerMu = Props::passHLT_mu20_iloose_L1MU15.get(evtinfo) ||
                        Props::passHLT_mu50.get(evtinfo);
    bool passTriggerEl = Props::passHLT_e24_lhmedium_iloose_L1EM20VH.get(evtinfo) ||
                        Props::passHLT_e60_lhmedium.get(evtinfo);

    if( isMC ){
        passTriggerEl |= Props::passHLT_e24_lhmedium_L1EM18VH.get(evtinfo);
        //passTriggerEl |= Props::passHLT_e24_lhvloose_L1EM18VH.get(evtinfo);
    }else{
        passTriggerEl |= Props::passHLT_e24_lhmedium_L1EM20VH.get(evtinfo);
        //passTriggerEl |= Props::passHLT_e24_lhvloose_L1EM20VH.get(evtinfo);
    }//end if-else

    if(!(( m_result.mu.size() > 0 && passTriggerMu )
       || (m_result.el.size() > 0 && passTriggerEl)) ){
        pass = false;
    }//end if : require that a lepton and corresponding event trigger match

    if (!pass) return false;
    if(!isKinVar) m_cutFlow.count("trigger");
	*/

    //if (!pass) return false;
    //m_result.pass = true;
    //return true;

	// Or even just pass the preSelection only, like in default DummyEvtSelection
    return passPreSelection(containers, isKinVar);

}//end passSelection function




///PROTECTED FUNCTIONS ADDED BY Hai Jiang

void EventSelection_Zemu::clearResult(){
    m_result.pass = true;
    m_result.el.clear();
    m_result.mu.clear();
    m_result.jets.clear();
    m_result.taus.clear();
	m_result.met = 0;
	m_result.truthParticles.clear();
}


int EventSelection_Zemu::doLeptonPreSelection(const xAOD::ElectronContainer* electrons,
                           const xAOD::MuonContainer* muons,
                           std::vector<const xAOD::Electron*>& el,
                           std::vector<const xAOD::Muon*>& mu){

    el.clear();
    mu.clear();
    int nelecs=0;
    int nmuons=0;

    for (unsigned int iElec = 0; iElec < electrons->size(); ++iElec) {
        const xAOD::Electron* elec = electrons->at(iElec);

        //if (!DBProps::passOR.get(elec)) continue;
        //if( Props::OROutputLabel.get(elec) ) continue; ///Reject the electron if it is an overlap object TODO : Check that this is done correctly.
        //if( !LLPProps::isFromPV.get(elec) ) continue; //Reject the electron if it doesn't come from the PV
        //if( fabs( Props::d0sigBL.get(elec) )>=5. ) continue;


		//printf( "Here is a check inside EventSelection_Zemu::doLeptonPreSelection Props::isLooseLH:  %i\n", Props::isLooseLH.get(elec));
        //if( !Props::isLooseLH.get(elec) ) continue; //Require at least a loose electron
        //if( fabs(LLPProps::ElClusterEta.get(elec)) < 1.52 && fabs(LLPProps::ElClusterEta.get(elec))>1.37 ) continue; //Reject electrons in the gap.

        nelecs++;
        el.push_back(elec);
    }//end for : loop over all electrons and apply pre-selection

    for (unsigned int iMuon = 0; iMuon < muons->size(); ++iMuon) {
        const xAOD::Muon * muon = muons->at(iMuon);

        //if ( Props::OROutputLabel.get(muon) ) continue;
        //if( ! LLPProps::isFromPV.get(muon) ) continue;
        //if( fabs( Props::d0sigBL.get(muon) )>=3. ) continue;
        //if( ! Props::isLooseTrackOnlyIso.get(muon) ) continue;

        nmuons++;
        mu.push_back(muon);
    }//end for : loop over all muons

    return nelecs + nmuons;
}//end doLeptonPreSelection


int EventSelection_Zemu::doJetPreSelection(const xAOD::JetContainer* jets){
    m_result.jets.clear(); //Reset this vector

    for(const xAOD::Jet * jet : *jets){
        //if( !Props::passOR.get(jet) ) continue; //Accept any jet which passes OR
        m_result.jets.push_back(jet);
    }//end for : loop over jets

    std::sort(m_result.jets.begin(), m_result.jets.end(), sort_pt);
    return m_result.jets.size();
}//end doJetPreSelection


int EventSelection_Zemu::doTauPreSelection(const xAOD::TauJetContainer* taus){
    m_result.taus.clear(); //Reset this vector

    for(const xAOD::TauJet * Taujet : *taus){
        //if( !Props::passOR.get(jet) ) continue; //Accept any jet which passes OR
        m_result.taus.push_back(Taujet);
    }//end for : loop over jets

    //std::sort(m_result.taus.begin(), m_result.taus.end(), sort_pt);
    return m_result.taus.size();
}//end doTauPreSelection






